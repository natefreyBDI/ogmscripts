# Author                 :  Sage Gonzales
# Created                :  23th April 2020
# Last Modified          :
# Modifications          :
# Version                :  1.0


# Description            :

# Teseted with           :
import subprocess
from nptdms import TdmsFile
from pathlib import Path
import os.path, time
import pandas as pd
import xlsxwriter
from datetime import date


class DataQC:
    range_l = []
    rpt_l = []

    def __init__(self, tdms_file):
        self.file = TdmsFile(tdms_file)
        self.tdms_data_group = self.file.groups()[0]
        self.tdms_channels = self.tdms_data_group.channels()

    def check_nan(self):

        nan = 0

        for ch in self.tdms_channels:
            for data_point in ch.data:
                data_point_str = str(data_point)
                if (data_point_str == "inf") or (data_point_str == "-inf") or (data_point == 0) or (
                        pd.isnull(data_point)):
                    nan += 1
        return nan

        # return "Total nans = %s " % nan

    def bad_sensor_dict(self):

        bad_sensor = []

        for ch in self.tdms_channels:
            ch_name = str(ch)
            ch_name = ch_name.replace('<', '')
            ch_name = ch_name.replace('>', '')
            ch_name = ch_name.replace("'", "")
            split_ch_name = ch_name.split('/')
            channel_name = str(split_ch_name.pop())

            for data_point in ch.data:

                data_point_str = str(data_point)
                if (data_point_str == "inf") or (data_point_str == "-inf") or (data_point == 0) or (
                        pd.isnull(data_point)):
                    bad_sensor.append(channel_name)

        bad_sensor_count = {i: bad_sensor.count(i) for i in bad_sensor}

        bs_count = ' '.join("{!s}: {!r} \n".format(key, val) for (key, val) in bad_sensor_count.items())

        return bs_count

    def out_of_range(self, rng):
        range_count = []
        range = {}

        ignored_channels = ['Time', 'WS-Precip', 'WS-Humid', 'WS-WindSpeed', 'WS-WindDirection', 'WS-Temp', 'Sn', 'Sm',
                            'Sx',
                            'Dn', 'Dm', 'Dx', 'Pa', 'Ta', 'Tp', 'Ua', 'Rc', 'Rd', 'Ri', 'Rp', 'TEMP_Average',
                            'TEMP_Max',
                            'TEMP_Min', 'WIND DIRECTION_Average', 'WIND DIRECTION_Max', 'WIND DIRECTION_Min']

        for ch in self.tdms_channels:
            ch_name = str(ch)
            ch_name = ch_name.replace('<', '')
            ch_name = ch_name.replace('>', '')
            ch_name = ch_name.replace("'", "")
            split_ch_name = ch_name.split('/')
            channel_name = str(split_ch_name.pop())

            if not any(chnl in channel_name for chnl in ignored_channels):

                for data_point in ch.data:
                    data_point_str = str(data_point)

                    if (data_point > 1000) or (data_point < -1000):

                        if data_point not in ignored_channels:
                            rng = True
                            range_count.append(data_point_str)
                            range.update({channel_name: data_point})

        # for k in range:
        #     if k == {}:
        #         del k

        self.range_l = ' '.join("{!s}: {!r} \n".format(key, val) for (key, val) in range.items())
        return rng

    def repeat(self, rpt):

        rep_sensor = {}

        ignored_channels = ['Time', 'WS-Precip', 'WS-Humid', 'WS-WindSpeed', 'WS-WindDirection', 'WS-Temp', 'Sn', 'Sm',
                            'Sx',
                            'Dn', 'Dm', 'Dx', 'Pa', 'Ta', 'Tp', 'Ua', 'Rc', 'Rd', 'Ri', 'Rp', 'TEMP_Average',
                            'TEMP_Max',
                            'TEMP_Min', 'WIND DIRECTION_Average', 'WIND DIRECTION_Max', 'WIND DIRECTION_Min']

        for ch in self.tdms_channels:
            ch_name = str(ch)
            ch_name = ch_name.replace('<', '')
            ch_name = ch_name.replace('>', '')
            ch_name = ch_name.replace("'", "")
            split_ch_name = ch_name.split('/')
            channel_name = str(split_ch_name.pop())

            if not any(chnl in channel_name for chnl in ignored_channels):
                if not rpt:  # Only check for repeating values if none have been found yet.
                    rptValueThreshold = 0.05
                    distinctValues = len(set(ch.data))
                    totalValues = len(ch.data)
                    if distinctValues < rptValueThreshold * totalValues:
                        rpt = True
                        rep_sensor.update({channel_name: ch.data[1]})

        self.rpt_l = ' '.join("{!s}: {!r} \n".format(key, val) for (key, val) in rep_sensor.items())
        return rpt


def call_one():
    row = 1
    col = 0

    today = date.today()

    data_folder = r'F:\Shares\Projects\data_checks'
    #decimated_folder = 'D:\\Shares\\Projects\\Project1\\decimated\\'

    pathlist = Path(data_folder).glob('**/*.tdms')
    workbook = xlsxwriter.Workbook('{} DataReport.xlsx'.format(today))
    worksheet = workbook.add_worksheet()

    for path in pathlist:
        str_path = str(path)
        qc_file = DataQC(str_path)
        pro_name = os.path.basename(str_path)

        bold = workbook.add_format({'bold': True})
        # Write some data headers.
        worksheet.write('A1', 'Project', bold)
        worksheet.write('B1', 'Number of nan', bold)
        worksheet.write('C1', 'Repeating values?', bold)
        worksheet.write('D1', 'Out of range values?', bold)

        worksheet.write(row, col, pro_name)
        worksheet.write(row, col + 1, qc_file.check_nan())
        worksheet.write(row, col + 2, qc_file.repeat(False))
        worksheet.write(row, col + 3, qc_file.out_of_range(False))
        worksheet.write_comment(row, col + 1, qc_file.bad_sensor_dict(), {'x_scale': 4, 'y_scale': 4})
        worksheet.write_comment(row, col + 2, qc_file.rpt_l, {'x_scale': 4, 'y_scale': 4})
        worksheet.write_comment(row, col + 3, qc_file.range_l, {'x_scale': 4, 'y_scale': 4})
        row += 1
        # worksheet.write_comment('{}'.format(qc_file.bad_sensor_dict()), {'x_scale': 4, 'y_scale': 4})

    workbook.close()


if __name__ == "__main__":
    call_one()
    print("done")
