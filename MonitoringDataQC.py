from nptdms import TdmsFile
from pathlib import Path

# Iterate through all of the data in each channel to look for #NV's and repeating values.
def checkForNaN(filePath,NaN,rpt):
    # Get file to parse for #NV's and repeating values.
    fileToCheck = TdmsFile.read(filePath)

    # Assign tdmsDataGroup based on [0] index as this is the way all BDI TDMS files are structured.
    tdmsDataGroup = fileToCheck.groups()[0]

    # Identify all data channels in the TDMS file.
    # tdmsChannels = fileToCheck.group_channels(tdmsDataGroup)

    # Create a list of Weather Station Channels to ignore.
    ignoredChannels = ['WS-Precip','WS-Humid','WS-WindSpeed','WS-WindDirection','WS-Temp','Sn','Sm','Sx','Dn','Dm','Dx','Pa','Ta','Tp','Ua','Rc','Rd','Ri','Rp']

    #for channelObject in tdmsChannels:
    for channelObject in tdmsDataGroup.channels():
        # Check for repeating Values
        channelString = str(channelObject)
        channelString = channelString.replace('<','')
        channelString = channelString.replace('>','')
        channelString = channelString.replace("'","")
        splitChanelString = channelString.split('/')
        channelName = str(splitChanelString.pop())

        if not any(chnl in channelName for chnl in ignoredChannels):
            if rpt == False:        # Only check for repeating values if none have been found yet.
                rptValueThreshold = 0.05
                distinctValues = len(set(channelObject.data))
                totalValues = len(channelObject.data)
                if distinctValues < rptValueThreshold*totalValues:
#                if len(channelObject.data) != len(set(channelObject.data)): # DEBUG!!!! - code is breaking right here because there are occasional repeating values in legitimate data streams
                    rpt = True
        # Check for NaN
        for dataPoint in channelObject.data:
            dataPointStr = str(dataPoint)
            if (dataPointStr == "inf") or (dataPointStr == "-inf"):
                NaN +=1
    return NaN,rpt

# Identify the folder to analyze.
folderToCheck = 'C:\\FilesToCheck\\'

# Retrieve a list of all files in that folder.
pathlist = Path(folderToCheck).glob('**/*.tdms')

# For each file in the list, analyze the data to identify missing or repeating values.
for path in pathlist:
    # Convert path to string.
    pathStr = str(path)                                         
    
    # Pass the path and initialize NaN and rpt, and assign the returned values to NaNcount and repeatingValues.
    NaNcount, repeatingValues = checkForNaN(pathStr,0,False)    
    
    # Assign text string to print to console based on the value of repeatingValues.
    if repeatingValues == True:
        strRpt = "Repeating values FOUND"
    else:
        strRpt = "Repeating values NOT FOUND"
    
    # Print the reults of the analysis to the console.
    print(str(NaNcount) + " NaN found and " + strRpt + " in " + pathStr)
